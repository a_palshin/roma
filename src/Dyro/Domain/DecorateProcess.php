<?php


namespace Dyro\Domain;

use Dyro\Domain\ProcessRequest;

abstract class DecorateProcess extends ProcessRequest
{
    protected $processRequest;

    public function __construct(ProcessRequest $processRequest)
    {
        $this->processRequest = $processRequest;
    }
}