<?php


namespace Dyro\Domain;

use RecursiveIteratorIterator;

abstract class ProcessRequest
{
    abstract function process(RequestHelper $helper, RecursiveIteratorIterator $json);
}