<?php


namespace Dyro\Domain;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class JsonExtractor
{
    public $jsonIterator;

    public function extract($json)
    {
        return $this->jsonIterator = new RecursiveIteratorIterator(
            new RecursiveArrayIterator(json_decode($json, TRUE)),
            RecursiveIteratorIterator::SELF_FIRST
        );
    }
}