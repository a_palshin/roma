<?php


namespace Dyro\Domain;

use RecursiveIteratorIterator;

class RecordTransaction extends DecorateProcess
{
    public function process(RequestHelper $helper, RecursiveIteratorIterator $json)
    {
        print __CLASS__ . " : записали в базу <br>";
        $this->processRequest->process($helper, $json);
    }
}