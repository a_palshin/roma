<?php


namespace Dyro\Domain;

use Dyro\Domain\Notifier\Notifier;
use RecursiveIteratorIterator;

class MainProcess extends ProcessRequest
{
    protected $notifier;

    public function process(RequestHelper $helper, RecursiveIteratorIterator $json)
    {
        $this->setNotifier();
        $this->notifier->notifyTransaction();
    }

    public function setNotifier()
    {
        $this->notifier = new Notifier();
    }
}