<?php


namespace Dyro\Domain;


class ValidateJSON
{
    public function isJson($str){
        return json_decode($str) != null;
    }
}
