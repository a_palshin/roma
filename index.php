<?php

require __DIR__ . '/vendor/autoload.php';

$json = <<< JSON
{
  "transaction":{
    "status":"pending",
    "message":"Требование на оплату счёта создано.",
    "type":"payment",
    "id":"8759cf84-e56d-44b7-a8ae-62640f6402c4",
    "uid":"8759cf84-e56d-44b7-a8ae-62640f6402c4",
    "order_id":"100000003495",
    "amount":22000,
    "currency":"BYR",
    "description":"Оплата заказа #123",
    "tracking_id":"AB8923",
    "created_at":"2015-12-07T14:21:24.420Z",
    "expired_at":"2016-12-07T14:21:240Z",
    "paid_at":"2016-12-07T14:40:120Z",
    "test":true,
    "payment_method_type":"erip",
    "customer":{
        "email":"ivanpetrov@example.com",
        "ip":"127.0.0.7"
    },
    "payment":{
        "ref_id":null,
        "message":null,
        "status":"pending",
        "gateway_id":1
    }
  }
}
JSON;

$validator = new \Dyro\Domain\ValidateJSON();

if($validator->isJson($json)) {
    $extractor = new \Dyro\Domain\JsonExtractor();
    $jsonData = $extractor->extract($json);
}

$process = new \Dyro\Domain\CheckTransaction(
                new \Dyro\Domain\RecordTransaction(
                    new \Dyro\Domain\MainProcess(
           )));
$process->process(new \Dyro\Domain\RequestHelper(), $jsonData);



